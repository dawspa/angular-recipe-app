import {NgModule} from '@angular/core';
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";

import {ShoppingListComponent} from "./shopping-list.component";
import {ShoppingEditComponent} from "./shopping-edit/shopping-edit.component";
import {SharedModule} from "../shared/shared.module";

@NgModule({
  imports: [
    FormsModule,
    SharedModule,
    RouterModule.forChild([
      {path: '', component: ShoppingListComponent}
    ])
  ],
  declarations: [ShoppingListComponent, ShoppingEditComponent]
})
export class ShoppingListModule {
}

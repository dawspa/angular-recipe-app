# RecipeBox

A place to keep and share tasty food recipes

App created with `Angular 8` and `NgRx`, powered on the backend by `Firebase`. Server-side rendered thanks to *Angular Universal*.

![recipebox screenshot](src/assets/recipebox.png)

## Live demo

Enter: https://recipebox-ds.herokuapp.com

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

**Unit tests** were defined for some of the components. Run them with the command below.

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

End to end **integration** tests were created to check app functionality. Run them with the command below.

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

Checkout to *cypress e2e* branch and run `ng e2e` to execute the end-to-end tests via [Cypress](https://www.cypress.io/).

## License info

Standard MIT License

Welcome page photo made by © **Monika Spałek**

**Thank You for viewing my project!**


